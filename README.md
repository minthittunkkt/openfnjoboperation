# OpenFn job with two operation

### Operation one (Making GET request)
Get the post information from [jsonplaceholder api](https://jsonplaceholder.typicode.com/).

### Operation two (Making POST request)
Send the requested data from operation one to [webhook site](https://webhook.site) via POST request.

### Build and run instruction

1. Clone the OpenFn/devtools. [OpenFn/devtools](https://github.com/OpenFn/devtools)
2. Run ./install.sh ssh or ./install.sh https to install core, language-common, and language-http
3. To install specific adaptors, run ./install.sh https language-http
4. Install openfn/core global, run npm install -g @openfn/core
5. Run execute command in your terminal


### Execute command for language-http adaptor

> Replace this path with your absolute file path.
> ~/Desktop/OpenFnDevTool/

```
core execute \
-l ~/Desktop/OpenFnDevTool/devtools/adaptors/language-http \
-s ~/Desktop/OpenFnDevTool/job/state.json \
-o ~/Desktop/OpenFnDevTool/job/output.json \
-e ~/Desktop/OpenFnDevTool/job/expression.js
```


