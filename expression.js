//Operation 1
//Taking the information from RESTful GET Api
get(
  'https://jsonplaceholder.typicode.com/posts/1',
  state => {
    return state;
  }
);

//Operation 2
//Sending the data to desire RESTful POST Api
post('https://webhook.site/82eabe94-c5bf-4098-ba84-cb1e3f3d47d7', {
  body: state => {
    return {
      title: dataValue('title')(state),
      body: dataValue('body')(state),
    };
  },
  headers: {
    'Content-Type': 'application/json',
  },
});

//Execute command
/*
core execute \
-l ~/Desktop/OpenFnDevTool/devtools/adaptors/language-http \
-s ~/Desktop/OpenFnDevTool/job1/state.json \
-o ~/Desktop/OpenFnDevTool/job1/output.json \
-e ~/Desktop/OpenFnDevTool/job1/expression.js
*/